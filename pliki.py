#zapis do pliku
#pierwszy parametr to po prostu nazwa pliku
#drugi parametr wskazuje, w jaki sposób plik
#będziemy traktować:
#r - plik będzie tylko do odczytu
#r+ - plik do odczytu i zapisu (kursor po owarciu stawia się w pozycji 0 - na początku pliku!)
#w - plik tylko do zapisu
#a - plik otwarty do zapisu lecz nie nadpisuje
#PONADTO W NOWSZYCH WYDANIACH
#a+ - plik do odczytu i zapisu, jednak kursor ustawia się na końcu pliku (dodaje do istniejących treści nowe)
# danych znajdujących się w pliku (tak działa w)
# lecz nowe dane dopisuje na końcu pliku
plik=open("nowyplik.txt",'w') #plik do zapisu; jeżeli plik nie istenieje - tworzy go
#kolejne wywołania write powodują zapis do pliku bajt po bajcie; by w pliku
#powstały nowe linie trzeba używać znaku specjalnego \n (nowa linia)
plik.write("To jest nowy zapis danych\n")
plik.write("Kolejny zapis\n")
plik.write("I kolejny\n")

plik.write("Nowa linia")
plik.close() #warto pamiętać o zamykaniu pliku gdy już nie potrzebujemy z niego
             #korzystać; w innym wypadku plik będzie "zastrzeżony" przez nasz
             #program i system, inni użytkownicy, a nawet nasz aktualny program
             #nie będą mieli dostępu do niego (np. do odczytu). 
plik=open("nowyplik.txt",'r') #ponieważ plik w postaci do zapisu został zamknięty
                              #teraz mamy możliwość użyć go ponownie (do odczytu)
for linia in plik:            #metoda pozwala na czytanie pliku linia po linii
     print(linia, end='')
#plik.read()   #w ten sposób możemy wczytać CAŁY plik do np. zmiennej (duże pliki - duża ilość RAM)
plik.close()   #zamknięcie pliku


plik=open("binarny.txt","wb")           #przykład zapisu binarnego; by zapisywać binarnie
                                        #(wszystkie wartości) wystarczy dołożyć literę b po literze sposobu dostępu do pliku
                                        #działa ze wszystkimi sposobami dostępu 
plik.write(b'663118\0x0411ffaad22Tekst')#ciąg znakowy zapisany w postaci binarnej (tylko Python 3+)
plik.close()                            #zamknięcie pliku
print("")
plik=open("binarny.txt","rb")           #otworzenie do odzytu binarnego
zm=plik.read().decode('ascii')          #dekodowanie binanej wartości do ciągu znakowego (kodowanie ascii)
plik.seek(6)             #przykład przemieszczania się po pliku (każdego typu) 6 oznacza o przesunięciu się do 6 bajtu w pliku
print(plik.read(1))      #odczyt dokładnie jednego bajtu licząc od ustawionej wcześniej pozycji; tym samym przesuwamy się o tyle bajtów do przodu
print(str(zm))      #tutaj przykład wyświetlenia wartości dekodowanej z wartości binarnej
                    #należy pamiętać, że najlepszym sposobem zapisu jest zawsze zpisywanie pliku w postaci binarnej (w postaci tekstowej można utracić niektóre właściwości i znaki specjalne)
plik.close()

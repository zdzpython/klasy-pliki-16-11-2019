#w ten sposób tworzymy klasy w języku Python
#wedle konwencji nazwa klasy powinna zaczynać się
#dużą literą; reszta ograniczeń taka sama jak przy
#zmiennych i funkcjach
class MojaKlasa:
    #poniższy tekst należy do części opisowej
    #naszej klasy i będzie widoczny jedynie w narzędziach
    #programistycznych oraz dokumentacji kodu
    """Definicja mojej nowej klasy"""
    #od teraz momżemy deklarować dowolne zminne (pola)
    #oraz funkcje (metody), które będą należały do
    #naszego nowego typu
    wartosc1=0
    ciagTekstowy=""
    zm=.0 #liczba zmiennoprzecinkowa, inicjalizacja 0
    #i tak generlanie ile zmiennych zechcemy

    #Python nie rozpoznaje czy dana funkcja(metoda)
    #jest dołączona do danej klasy. W związku z
    #powyższym wszystkie metody w klasie MUSZĄ posiadać
    #parametr self NA PIERWSZEJ POZYCJI argumentów
    #w metodzie; inaczej nasz kod nie będzie działał
    def sumuj(self, a,b):
        return a+b
    def info(self):
        print("Moja własna nowa klasa!")
    def pustaFunkcja(self):
        pass
    #tak jak i pól, możemy mieć dowolną liczbę
    #metod, które zadeklarujemy i zdefiniujemy

    #tutaj przykład metody odwołującej się do
    #pól klasy; zawsze musi następować odwołanie
    #przez self!
    def wypiszZmienna(self):
        self.ciagTekstowy="Funkcja wywołująca składową własnej klasy"
        return self.ciagTekstowy

    #w klasie mamy nie tylko deklarowane przez nas składowe
    #ale również dziedziczone po głównym obiekcie języka
    #Python. Są to domyślne metody, które mają wykonywać się
    #w określonych sytuacjach, by dopasować działanie czyjegoś
    #kodu do naszego typu. Składowe te można wyświetlić
    #(wraz z tymi przez nas utworzonymi) poprzez funkcję
    #dir()
    #Każdą domyślną metodę możemy nadpisać. Oznacza to, że
    #możemy zmienić jej definicję na własną. Przykładawo
    #domyślnie próba przystosowanie naszej klasy do ciągu
    #znakowego skutkuje wyświetleniem jej nazwy, typu oraz
    #numeru komórki pamięci RAM, w której znajduje się
    #początek jej kodu

    def __str__(self):
        return "To jest własny typ danych!"
    #teraz wywołanie samej nazwy zmiennej naszej klasy
    #spowoduje wyświetlenie powyższego tekstu zamiast
    #domyślnego
    #przedefiniowanie zachowania obiektu w chwili wywołania
    #funkcji dir(obiekt)
    def __dir__(self):
        return ["To jest dir", "Nie ma podglądania metod"]
    #możemy też przedefiniować sam konstruktor klasy
    #czyli metodę, która wywoływana jest w chwili
    #tworzenia obiektu z naszej klasy:
    def __init__(self,param1="Nowy",param2="parametr",param3=12):
        #poniże dwie składowe (a, kolejna) zostają
        #utworzone (zdefiniowane) właśnie w konstruktorze
        #od tego momentu będą one widoczne w całym
        #obiekcie, będzie się można do nich
        #odwołać i je zmieniać/łączyć
        self.a=param1
        self.kolejna=param2
        #tutaj następuje przypisanie wartości do pola
        #już istniejącego w klasie (wcześniejsza
        #deklaracja i definicja)
        self.wartosc1=param3

class Inna:
    def __init__(self, x, y):
        #w konstruktorze powstają dwie zmienne
        #należące do klasy; gdy kontruktor się wykona
        #obiekt będzie posiadać dwie nowe składowe
        #(obiekt będzie je posiadał przez cały okres życia
        self.x=x
        self.y=y
    #KAŻDA metoda może tworzyć nowe składowe w obrębie
    #własnej klasy; to cecha języków parsowanych i
    #często określana jest mianem dynamicznego typowania
    #zmiennych badź dynamiczną edycją obiektów
    def dodajZmienna(self):
        self.pt=[self.x,self.y]

zmienna=MojaKlasa() #deklaracja obiektu na podstawie
                    #klasy

wynik=zmienna.sumuj(15,20) #należy zauważyć, że parametr
                           #self nie jest przez nas podawany
                           #uzupełnia go za nas interpreter
print(wynik) #wyświetlenie wyniku zwróconego przez sumuj
print(zmienna.wypiszZmienna())

print(dir(zmienna)) #ponieważ nadpisaliśmy tę metodę wywołana funkcja
                    #dir wyświetli tablicę dwóch wartości ciągu znakowego
                    #zamiast listy wszystkich dostępnych składowych klasy

print(zmienna)  #ponieważ nadpisaliśmy metodę __str__ zobaczymy napis,
                #jaki sami ustaliliśmy; domyślnie mielibyśmy inforamcję
                #w którym module została zdefiniowana nasza klasa, jakiego
                #nadrzędnego typu jest oraz w której komórce pamięci RAM
                #rozpoczyna się jej kod

print(zmienna.a,zmienna.kolejna)    #wyświetlą się wartości zmiennych a oraz kolejna
print(zmienna.wartosc1)             #wyświetli się zawartość zmiennej wartosc1

#ponieważ konstruktor posiada zdefiniowane paramery wejściowe (nadpisaliśmy
#konstruktor - __init__) to możemy zmieniać ich wartości tak, jakbyśmy
#odwoływali się do standardowej funkcji; ponieważ podajemy wartości
#w taki sposób, w jaki zostały podane w konstruktorze to nie musimy
#podawać nazw parametrów-zmiennych
nowaZmienna=MojaKlasa("Zdefiniowany własny","tekst",89)
#funkcje pozwalają na sprawdzenie, czy wartości zostały ustawione poprawnie
print(nowaZmienna.a,nowaZmienna.kolejna)
print(nowaZmienna.wartosc1)

#utworzenie kolejnej zmiennej z naszej klasy - tym razem zmieniamy wartość
#tylko jednego parametru wejściowego, dlatego podajemy jego nazwę i
#przypisujemy konkretną wartość (może to być wartść innej zmiennej)
kolejnaZmienna=MojaKlasa(param3=178)
print(kolejnaZmienna.a,kolejnaZmienna.kolejna)
print(kolejnaZmienna.wartosc1)
    
#punkt=Inna() #tutaj, jeżeli nie podamy parametrów do kod się nie wykona (brak dwóch parametrów)    
#ponieważ tutaj nasza klasa wymaga, by init (konstruktor)
#posiadał dwa parametry - x i y, a które NIE posiadają
#domyślnych wartości
punkt=Inna(12,67) #ten kod zadziała
print(dir(punkt)) #wypisanie składowych klasy
print(punkt.x,punkt.y)
punkt.dodajZmienna() #wywołanie tej metody powoduje dodanie do obiektu nowego pola - pt
print(dir(punkt)) #wypis tej funkcji pokazuje, że pole pt jest dostępne w obiekcie
print(punkt.pt) #wypisujemy zawartość pola pt -> jest to możliwe!
